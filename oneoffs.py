from flask import Flask, render_template, request, url_for
import urllib

app = Flask(__name__)

if app.debug is not True:
	import logging
	from logging.handlers import RotatingFileHandler
	file_handler = RotatingFileHandler('/srv/www/python/log/flask.log', maxBytes=1024 * 1024 * 100, backupCount=20)
	file_handler.setLevel(logging.INFO)
	formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
	file_handler.setFormatter(formatter)
	app.logger.addHandler(file_handler)


from datetime import date
from dateutil import parser
from dateutil.relativedelta import *
def calculateDuration(startDate1,endDate1="",startDate2="",endDate2=""):
  if startDate1=="": return ""
  sdate1 = parser.parse(startDate1)
  if endDate1=="":
    edate1 = date.today()
  else:
    edate1 = parser.parse(endDate1)
  delta = relativedelta(edate1,sdate1)

  if startDate2 != "":
    sdate2 = parser.parse(startDate2)
    if endDate2=="":
      edate2 = date.today()
    else:
      edate2 = parser.parse(endDate2)
    startDateTemp = sdate2 - delta
    delta = relativedelta(edate2,startDateTemp)
  return "%i years, %i months, %i days" % (delta.years, delta.months, delta.days)

@app.route("/")
def handleRoot():
	return render_template('index.html')

import age
@app.route("/age")
def handleAge():
	amy = age.ageString("July 24, 2013")
	beth = age.ageString("May 8, 2011")
	katya = age.ageString("September 9, 2008")
	esther = age.ageString("February 15, 2007")
	return render_template('age.html', AmyAge=amy, BethAge=beth, KatyaAge=katya, EstherAge=esther)


import diceware, os
@app.route("/diceware", methods=['POST','GET'])
def handleDiceware():
	wordCount=int(request.form.get("count","0"))
	special=False
	grid=False

	def ensure_dir(path):
	    """Ensure that path is a directory creating it if necessary.

	    If path already exists and is not a directory, print an error
	    message and quit with sys.exit().

	    Parameters:
	      path   String specifying the path to ensure

	    Return value:
	      path

	    """
	    if not os.path.exists(path):
	        os.makedirs(path)
	    elif not os.path.isdir(path):
	        print("error: '%s' is not a directory" % path)
	        sys.exit(1)
	    return path


	def config_default(config, section, option, default):
	    """Set default values for options that do not have a value."""
	    try:
	        config.get(section, option)
	    except NoSectionError:
	        config.add_section(section)
	        config.set(section, option, default)
	    except NoOptionError:
	        config.set(section, option, default)


	config_dir = ensure_dir(os.path.join(os.getcwd(),".diceware.py"))
	cache_dir = ensure_dir(os.path.join(config_dir, "cache"))

	word_list = diceware.get_word_list(cache_dir, "en")
	if not grid:
		words, with_specials = diceware.generate(word_list, wordCount, special)
		passphrase = " ".join(words)
		if special > 0:
			passphrase = " ".join(with_specials)
	else:
		words, length = diceware.generate_grid(word_list, wordCount, special)
		for word_row in words:
			passphrase += " ".join([word.ljust(length) for word in word_row]) + "\n"

	return render_template("diceware.html",count=wordCount,phrase=passphrase)

@app.route("/customers")
def customers():
  ksc = calculateDuration("June 23, 2005","September 07, 2010","November 04, 2011","")
  talbert = calculateDuration("September 27, 2007","")
  kpoh = calculateDuration("July 14, 2009","December 07, 2011")
  chop = calculateDuration("August 19, 2010","")
  ghc = calculateDuration("April 26, 2011","")
  caromont = calculateDuration("May 21, 2014","")
  return render_template('customers.html', kscTenure=ksc, tmgTenure=talbert, kpohTenure=kpoh,chopTenure=chop,ghcTenure=ghc,caromontTenure=caromont)

import coverArt
@app.route("/iTunesCovers/<title>")
def handleiTunesCovers(title):
	return render_template('iTunesCovers.html',
		title=title,
		covers=coverArt.getiTunesImages(title),
		googleLink=url_for('handleGoogleCovers', title=title)
	)

@app.route("/GoogleCovers/<title>")
def handleGoogleCovers(title):
	return render_template('GoogleCovers.html',
		title=title,
		covers=coverArt.getGoogleImages(title),
		encodedTitle=urllib.quote_plus(title)
	)

import bookParse, json
@app.route("/BookSearch/<title>")
def handleBookSearch(title):
	finds = {}
	finds["amazon"] = bookParse.checkAmazon(title)
	finds["goodreads"] = bookParse.checkGoodreads(title)
	return json.dumps(finds)

if __name__ == "__main__":
	app.debug = True
	app.run()
