#!/bin/sh

### BEGIN INIT INFO
# Provides:          uwsgi-flask
# Required-Start:    $all
# Required-Stop:     $all
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: starts the uwsgi app server
# Description:       starts uwsgi app server using start-stop-daemon
### END INIT INFO

PATH=/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
DAEMON=/usr/local/bin/uwsgi

OWNER=jmartin

NAME="python-oneoffs"
DESC="uwsgi application"
PIDFILE="/srv/www/python/tmp/uwsgi.pid"
LOGFILE="/srv/www/python/log/uwsgi.log"
SOCKET="/srv/www/python/tmp/uwsgi.sock"
RELOAD="/srv/www/python/tmp/reload"
APPDIR="/home/jmartin/development/flask-oneoffs"

test -x $DAEMON || exit 0

# Include uwsgi defaults if available
if [ -f /etc/default/uwsgi ] ; then
        . /etc/default/uwsgi
fi

set -e

DAEMON_OPTS="${APPDIR}/config/flask-oneoffs.ini --pidfile ${PIDFILE} --daemonize ${LOGFILE} -s ${SOCKET} --touch-reload ${RELOAD}"

case "$1" in
  start)
        echo -n "Starting $DESC: "
        start-stop-daemon --start --chuid $OWNER:$OWNER --user $OWNER \
                --exec $DAEMON -- $DAEMON_OPTS
        echo "$NAME."
        ;;
  stop)
        echo -n "Stopping $DESC: "
        ${DAEMON} --stop "${PIDFILE}"
        echo "$NAME."
        ;;
  reload)
        $DAEMON --reload "${PIDFILE}"
        ;;
  force-reload)
        killall -15 $DAEMON
       ;;
  restart)
        echo -n "Restarting $DESC: "
        stop
        start
        echo "$NAME."
        ;;
  status)  
        killall -10 $DAEMON
        ;;
      *)  
            N=/etc/init.d/$NAME
            echo "Usage: $N {start|stop|restart|reload|force-reload|status}" >&2
            exit 1
            ;;
    esac
    exit 0
