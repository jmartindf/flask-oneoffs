from datetime import date
from dateutil import parser
from dateutil.relativedelta import *

def ageString(birthdayString):
	bday = parser.parse(birthdayString)
	delta = relativedelta(date.today(),bday)
	suffix = ""
	if (delta.years == 0) or (delta.years < 2 and delta.months <=6):
		months = (delta.years*12) + delta.months
		return "%i months (%d days)" % (months,delta.days)
	if (delta.years == 1 and delta.months > 6) or delta.years > 1:
		if delta.years < 10 and delta.months >= 6:
			suffix = " and a half"
		return "%i%s years (%i months)" % (delta.years, suffix, delta.months)

