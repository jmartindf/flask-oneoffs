flask-oneoffs
=============

Simple little web scripts, implemented as one combined Flask application.

## Scripts

### Age

Calculate the ages of each of my children. Display them in a socially acceptible format.

## Steps to build my virtualenv

### Shell Configuration
    export WORKON_HOME=$HOME/.virtualenvs
    export PROJECT_HOME=$HOME/development
    export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python3.3
    source /usr/local/bin/virtualenvwrapper.sh

### Commands

    mkvirtualenv flask-oneoffs
    pip install -r requirements.txt
