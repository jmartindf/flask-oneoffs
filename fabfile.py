from __future__ import with_statement
from fabric.api import *
from fabric.contrib.console import confirm

env.hosts = ['jmartin@mark.desertflood.com']

def test():
	local("./manage.py test my_app")

def commit():
	local("git add -i && git commit")

def push():
	local("git push origin")

def deploy():
	push()
	code_dir = "/home/jmartin/development/flask-oneoffs"
	with settings(warn_only=True):
		if run("test -d %s" % code_dir).failed:
			run("git clone git@github.com:jmartindf/flask-oneoffs.git %s" % code_dir)
	with cd(code_dir):
		run("git pull origin")
		run("touch /srv/www/python/tmp/reload")
