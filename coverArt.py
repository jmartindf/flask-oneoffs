import json, string, requests

# requestURL = http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/wa/wsSearch?term='Starship%20Troopers'&country=us&entity='ebook'
# name = urllib.urlencode("Starship Troopers")

def getiTunesImages(title):
	searchURL = "http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/wa/wsSearch"
	payload = {
		"term": title,
		"country": "us",
		"entity": "ebook"
	}
	r = requests.get(searchURL, params=payload)
	images = []
	#json_data=open("1.txt.js").read()
	parse_json=json.loads(r.text)
	for result in parse_json['results']:
		images.append(
			{
				"regular": result['artworkUrl100'].replace('100x100','600x600'),
				"hires": result['artworkUrl100'].replace('100x100bb','100000x100000-999')
			}
		)
	return images

def getGoogleImages(title):
	searchURL = "https://www.googleapis.com/books/v1/volumes"
	bookURL = "https://www.googleapis.com/books/v1/volumes/"
	apiKEY = "AIzaSyALSxP1EPFZLxDR2AY2ZRidj3Y5STZQLWg" 
	payload = {
		"q": "intitle:"+title,
		"key": apiKEY
	}
	r = requests.get(searchURL, params=payload)
	allImages = []
	parse_json=json.loads(r.text)
	for result in parse_json['items']:
		r2 = requests.get(bookURL+result["id"], params={"key":apiKEY})
		json2 = json.loads(r2.text)
		imageData = {}
		try:
			images = json2["volumeInfo"]["imageLinks"]
			imageData["medium"] = images["medium"]
			imageData["large"] = images["large"]
			imageData["extraLarge"] = images["extraLarge"]
		except KeyError, e:
			pass
		if imageData:
			allImages.append(imageData)
	return allImages
