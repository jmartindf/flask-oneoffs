from bs4 import BeautifulSoup
import re
import json
import urllib
import requests

def checkAmazon(title):
	headers = requests.utils.default_headers()
	headers.update(
	    {
	        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0.2 Safari/602.3.12',
	    }
    )
	searchURL = "https://www.amazon.com/s/ref=nb_sb_noss_2"
	payload = {
		"url": "search-alias=digital-text",
		"field-keywords": title
	}
	r = requests.get(searchURL, headers=headers, params=payload)
	#html_doc = open("kindleSearchResults.html","r").read()

	soup = BeautifulSoup(r.text, 'lxml')
	results = {}
        cnt = 0

	try:
		# Check for a hero result. Ugh.
		details = {}
		hero = soup(name="div",class_="sx-hero-container")
		div = hero[0].find_next(name="div",class_="a-spacing-mini")
		book = div(name="a",class_="a-text-normal",limit=1)
		book = book[0]
		details["title"] = book["title"].encode('utf-8')
		url = book["href"].encode('utf-8')		
		url = re.sub("\/ref.*","/", url)
		details["url"] = url
		currency = book.find_next(name="sup",class_="sx-price-currency").text.encode('utf-8')
		whole = book.find_next(name="span",class_="sx-price-whole").text.encode('utf-8')
		fractional = book.find_next(name="sup",class_="sx-price-fractional").text.encode('utf-8')
		details["price"] = "%s%s.%s" % (currency,whole,fractional)
                cnt = cnt + 1
                results["(%s) %s" % (cnt, details["title"])] = details
	except (AttributeError, KeyError, IndexError) as err:
		pass

	try:
		books = soup(name="a",class_="s-access-detail-page",limit=5)
		for book in books:
			details = {}
			details["title"] = book["title"].encode('utf-8')
			url = book["href"].encode('utf-8')		
			url = re.sub("\/ref.*","/", url)
			details["url"] = url
			currency = book.find_next(name="sup",class_="sx-price-currency").text.encode('utf-8')
			whole = book.find_next(name="span",class_="sx-price-whole").text.encode('utf-8')
			fractional = book.find_next(name="sup",class_="sx-price-fractional").text.encode('utf-8')
			details["price"] = "%s%s.%s" % (currency,whole,fractional)
                        cnt = cnt + 1
                        results["(%s) %s" % (cnt, details["title"])] = details
	except (AttributeError, KeyError, IndexError) as err:
		pass
	return results

def checkGoodreads(title):
	searchURL = "https://www.goodreads.com/search/index.xml"
	payload = {
		"key": "rojeE90fL8BE2pQk3YpOQ",
		"q": title
	}
	r = requests.get(searchURL, params=payload)

	#xml_doc = open("goodreads.xml","r").read()

	soup = BeautifulSoup(r.text, 'lxml-xml')
	results = {}
        cnt = 0

	try:
		books = soup(name="work",limit=5)
		for book in books:
			details = {}
			work = book.find_next("best_book")
			bookid = work.find_next("id").text
			title = work.find_next("title").text.encode('utf-8')
			url = "https://www.goodreads.com/book/show/%s.%s" % (bookid,urllib.quote_plus(title.replace(" ","_")))
			details["title"] = title
			details["url"] = url
                        cnt = cnt + 1
                        results["(%s) %s" % (cnt, details["title"])] = details
	except (AttributeError, KeyError, IndexError) as err:
		pass
	return results



